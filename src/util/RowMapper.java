package util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface RowMapper {
	public Object rowMapping(ResultSet rs) throws SQLException;
	
}
